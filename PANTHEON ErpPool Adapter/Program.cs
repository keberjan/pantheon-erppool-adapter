﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace PANTHEON_ErpPool_Adapter
{
    class Program
    {
        static void Main(string[] args)
        {

            string query;
            query = "SELECT * FROM tHE_MoveItem";
            SqlConnection myConnection = new SqlConnection("server=KEB-WRK-SL\\DATALAB55;" + "trusted_connection=yes;" + "database=DataLabMF_Anuiteta;" + "connection timeout = 30");
            SqlCommand cmd = new SqlCommand(query, myConnection);

            try
            {
                myConnection.Open();
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Cannot connect to Ms SQL database");
                var debugKey = Console.ReadKey();

                switch (debugKey.Key)
                {
                    case ConsoleKey.D:
                        Console.WriteLine(ex);
                        Console.ReadLine();

                        FileStream ostrm;
                        StreamWriter writer;
                        TextWriter oldOut = Console.Out;

                        try
                        {
                            ostrm = new FileStream("./error.log", FileMode.OpenOrCreate, FileAccess.Write);
                            writer = new StreamWriter(ostrm);
                            writer.BaseStream.Seek(0, SeekOrigin.End);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Cannot write to error.log. Check file permissions!");
                            Console.WriteLine(e.Message);
                            return;
                        }

                        Console.SetOut(writer);
                        Console.WriteLine("\n\n" + ex);

                        Environment.Exit(0);
                        break;

                    default:
                        Console.WriteLine();
                        break;
                }
            }

            Console.WriteLine("Connected to MS SQL database!");

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                String acKey = reader["acKey"].ToString();
                String anNo = reader["anNo"].ToString();

                Racun racun = new Racun();
                racun.acKey = acKey;
                racun.anNo = anNo;

                string json = JsonConvert.SerializeObject(racun);
                Console.WriteLine(json);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost/json.php");
                httpWebRequest.ContentType = "text/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Console.WriteLine(result);
                    Console.WriteLine();
                }
            }
            
            Console.ReadLine();
        }
    }

    public class Racun
    {
        public string acKey { get; set; }
        public string anNo { get; set; }
    }
}
